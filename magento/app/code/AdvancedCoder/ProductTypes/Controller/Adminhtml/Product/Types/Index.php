<?php

namespace AdvancedCoder\ProductTypes\Controller\Adminhtml\Product\Types;

use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;

class Index extends Action
{
    /**
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Advanced_Coder::product_types';

    public function execute(): ResultInterface
    {
        /** @var Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->setActiveMenu('AdvancedCoder_ProductTypes::advanced_coder')
            ->addBreadcrumb(__('Product Types'), __('List'));
        $resultPage->getConfig()->getTitle()->prepend(__('Product Types'));

        return $resultPage;
    }
}
