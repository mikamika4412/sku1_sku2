<?php

namespace AdvancedCoder\ProductTypes\Observer;

use \Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;
use AdvancedCoder\ProductTypes\Model\Config;


class CustomPrice implements ObserverInterface
{
    private $config;
    public function __construct(Config $config)
    {
        $this->config=$config;

    }
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $sku1s = $this->config->getSku1();
        $sku2s = $this->config->getSku2();
        //get the item just added to cart
        $item = $observer->getEvent()->getData('quote_item');
        $product = $observer->getEvent()->getData('product');
        //(optional) get the parent item, if exists
        $item = ($item->getParentItem() ? $item->getParentItem() : $item);

        // set your custom price
        if ($item->getSku() === $sku2s) {

            $price = 0;
            $item->setCustomPrice($price);
            $item->setOriginalCustomPrice($price);
            $item->getProduct()->setIsSuperMode(true);
        }




    }
}
