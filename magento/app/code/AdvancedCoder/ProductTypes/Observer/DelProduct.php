<?php

namespace AdvancedCoder\ProductTypes\Observer;
use \Magento\Framework\Event\ObserverInterface;
use AdvancedCoder\ProductTypes\Model\Config;

class DelProduct implements ObserverInterface
{
    protected $request;
    protected $cart;
//    static  $sku1Qty=0;
    private $config;


    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Checkout\Model\CartFactory $cart,
        \Magento\Quote\Model\Quote\Item\Repository $QuoteRepository,
        Config $config
    ) {
        $this->cart = $cart;
        $this->request = $request;
        $this->quoteRepository = $QuoteRepository;
        $this->config=$config;
    }
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
//        $items = $observer->getCart()->getQuote()->getItems();
//        $cartId= $observer->getCart()->getId();
//        $items = $this->cart->create()->getItems();
        $sku1s = $this->config->getSku1();
        $sku2s = $this->config->getSku2();
        $items = $this->quoteRepository->getList($observer->getCart()->getQuote()->getId());

        $existMainProduct = false;
        foreach ($items as $item_obj) {
            $item = $item_obj->getData();
//            if ($item['sku'] == '24-MB01') {
            if ($item['sku'] == $sku1s) {
                $existMainProduct = true;
            }
            if ($item['sku'] == $sku2s && !$existMainProduct) {
//            if ($item['sku'] == '24-MB04' && !$existMainProduct) {
                $this->quoteRepository->deleteById($item_obj->getData('quote_id'), $item_obj->getId());
            }
        }

//        foreach ($items as $item) {
////            $item_array=$item->getData();
//            if($item_array['sku'] == '24-MB04' && !$existMainProduct){
////            if(!$existMainProduct){
//                $item->delete();
//            }
//        }

    }
}
/*
$items = $observer->getCart()->getQuote()->getItems();

foreach ($items as $item) {
    $item_array=$item->getData();
    if($item_array['sku']=='24-MB04' && $item_array['qty']>1){
        $item->setQty(1);
        $item->save();
    }
}
*/
