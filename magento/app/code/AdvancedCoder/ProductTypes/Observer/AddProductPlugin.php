<?php

namespace AdvancedCoder\ProductTypes\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Backend\App\Action;
use Magento\Checkout\Model\Session;
use AdvancedCoder\ProductTypes\Model\Config;

class AddProductPlugin implements ObserverInterface
{
    protected $_productRepository;
    protected $_cart;
    protected $formKey;
    static $QtySku2s;
    private $config;

    public function __construct(
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Framework\Data\Form\FormKey $formKey,
        \Magento\Checkout\Model\Session $checkoutSession,
        Config $config

    ) {
        $this->_productRepository = $productRepository;
        $this->_cart = $cart;
        $this->formKey = $formKey;
        $this->checkoutSession = $checkoutSession;
        $this->config=$config;
    }

    public function getQuotes()
    {
        return $this->checkoutSession->getQuote();
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $sku1s = $this->config->getSku1();
        $sku2s = $this->config->getSku2();
        $item = $observer->getEvent()->getData('quote_item');
        $product = $observer->getEvent()->getData('product');
        $item = ($item->getParentItem() ? $item->getParentItem() : $item);

        // Enter the id of the prouduct which are required to be added to avoid recurrssion
        $allItems = $this->getQuotes()->getAllVisibleItems();
        foreach ($allItems as $item) {
            if ($item->getSku() === ($sku2s)) {
                self::$QtySku2s = $item->getQty();
            }
        }

        if ($item->getSku() === $sku1s && self::$QtySku2s<1) {
            $_product = $this->_productRepository->get($sku2s);
            $allItems = $this->getQuotes()->getAllVisibleItems();
            $params = array(
                'qty' => 1
            );
//            var_dump($_product);
            $this->_cart->addProduct($_product, $params);
            $this->_cart->save();
        }
    }

}
