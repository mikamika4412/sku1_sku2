<?php

namespace AdvancedCoder\ProductTypes\Observer;

use AdvancedCoder\ProductTypes\Model\Config;


class UpdateCartItem implements \Magento\Framework\Event\ObserverInterface
{
    private $config;

    /**
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;

    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $sku1s = $this->config->getSku1();
        $sku2s = $this->config->getSku2();
        $items = $observer->getCart()->getQuote()->getItems();

        foreach ($items as $item) {
            $item_array = $item->getData();
            if ($item_array['sku'] == $sku2s && $item_array['qty'] > 1) {
                $item->setQty(1);
                $item->save();


            }
        }
    }
}

