<?php

namespace AdvancedCoder\ProductTypes\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{

    const XML_PATH_SKU1 = 'mika/general/SKU1';
    const XML_PATH_SKU2 = 'mika/general/SKU2';


    public function getConfigValue($field)
    {
        return $this->scopeConfig->getValue(
            $field);
    }

    public function getGeneralConfig($code, $storeId = null)
    {

        return $this->getConfigValue(self::XML_PATH_SKU1);
    }

}
