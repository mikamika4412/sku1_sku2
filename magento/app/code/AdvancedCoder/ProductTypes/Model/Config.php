<?php

namespace AdvancedCoder\ProductTypes\Model;

//use Magento\Framework\App\Helper\AbstractHelper;
//use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

class Config
{

    const XML_PATH_SKU1 = 'mika/general/SKU1';
    const XML_PATH_SKU2 = 'mika/general/SKU2';

    private $config;

    public function __construct(ScopeConfigInterface $config)
    {
        $this->config = $config;
    }

    public function getSku1()
    {
        return $this->config->getValue(self::XML_PATH_SKU1);
    }

    public function getSku2()
    {
        return $this->config->getValue(self::XML_PATH_SKU2);
    }

}
