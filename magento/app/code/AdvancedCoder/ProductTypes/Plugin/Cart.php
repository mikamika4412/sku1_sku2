<?php

namespace AdvancedCoder\ProductTypes\Plugin;

class Cart
{
    /**
     * @param \Magento\Checkout\Model\Cart $subject
     * @param \Closure $proceed
     * @param $productInfo
     * @param $requestInfo
     * @return mixed
     */
    public function aroundAddProduct(
        \Magento\Checkout\Model\Cart $subject,
        \Closure $proceed,
        $productInfo,
        $requestInfo = null
    ) {
            $requestInfo['qty'] = 1;
            $result = $proceed($productInfo, $requestInfo);
        return $result;
    }
}
