<?php

namespace AdvancedCoder\Brend\Ui\Component\Control\Brend;

use AdvancedCoder\Brend\Model\BrendRepository;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\UrlInterface;

class GenericButton
{
    private UrlInterface $urlBuilder;
    private RequestInterface $request;
    private BrendRepository $productTypesRepository;

    /**
     * @param UrlInterface $urlBuilder
     * @param RequestInterface $request
     * @param BrendRepository $productTypesRepository
     */
    public function __construct(
        UrlInterface $urlBuilder,
        RequestInterface $request,
        BrendRepository $productTypesRepository
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->request = $request;
        $this->productTypesRepository = $productTypesRepository;
    }

    /**
     * @param $route
     * @param $params
     * @return string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->urlBuilder->getUrl($route, $params);
    }

    /**
     * @return int|mixed|null
     * @throws NoSuchEntityException
     */
    public function getBrend()
    {
        $productTypeId = $this->request->getParam('id');
        if ($productTypeId === null) {
            return 0;
        }
        $productType = $this->productTypesRepository->get($productTypeId);

        return $productType->getId() ?: null;
    }
}
