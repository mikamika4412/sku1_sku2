<?php

namespace AdvancedCoder\Brend\Ui\DataProvider\Brend;

use AdvancedCoder\Brend\Model\ResourceModel\Brend\CollectionFactory;
use Magento\Ui\DataProvider\AbstractDataProvider;

class EditDataProvider extends AbstractDataProvider
{
    /**
     * @param $name
     * @param $primaryFieldName
     * @param $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name, $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $collectionFactory->create();
        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldName,
            $meta,
            $data
        );
    }

    /**
     * @return array
     */
    public function getDataSourseData(){
        return [];
    }

    /**
     * @return array
     */
    public function getData()
    {
        return parent::getData();
    }

    /**
     * @return array
     */
    public function getMeta(): array
    {
        return $this->meta;
    }
}
