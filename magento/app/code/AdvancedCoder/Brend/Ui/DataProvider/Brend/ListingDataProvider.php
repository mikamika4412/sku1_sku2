<?php

namespace AdvancedCoder\Brend\Ui\DataProvider\Brend;

use AdvancedCoder\Brend\Model\ResourceModel\Brend\CollectionFactory;
use Magento\Ui\DataProvider\AbstractDataProvider;


class ListingDataProvider extends AbstractDataProvider
{
    private CollectionFactory $collectionFactory;

    /**
     * @param $name
     * @param $primaryFieldName
     * @param $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param array $meta
     * @param array $data
     */
    public function __construct(
       $name,
       $primaryFieldName,
       $requestFieldName,
       CollectionFactory $collectionFactory,
       array $meta = [],
       array $data = []
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->collection = $collectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * @return array
     */
    public function getData()
    {
        if (!$this->getCollection()->isLoaded()) {
            $this->getCollection()->load();
        }
        $items = $this->getCollection()->toArray();

        return $items;
    }
}

