<?php

namespace AdvancedCoder\Brend\Model;

use AdvancedCoder\Brend\Api\Data\BrendInterface;
use Magento\Framework\Model\AbstractModel;

class Brend extends AbstractModel implements BrendInterface
{

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_init('AdvancedCoder\Brend\Model\ResourceModel\Brend');
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->getData(BrendInterface::NAME);
    }

    /**
     * @param string $name
     * @return void
     */
    public function setName(string $name): void
    {
        $this->setData(BrendInterface::NAME, $name);
    }

    /**
     * @return mixed|null
     */
    public function getId()
    {
        return $this->_getData('id');
    }

    /**
     * @return mixed|null
     */
    public function getImage()
    {
        return $this->_getData('image');
    }

    /**
     * @param mixed $value
     * @return $this
     */
    public function setId($value)
    {
        $this->setData('id', $value);
        return $this;
    }

}
