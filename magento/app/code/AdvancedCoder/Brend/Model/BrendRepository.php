<?php

namespace AdvancedCoder\Brend\Model;

use AdvancedCoder\Brend\Api\Data\BrendInterface;
use AdvancedCoder\Brend\Api\BrendRepositoryInterface;
use AdvancedCoder\Brend\Api\BrendSearchResultInterface;
use AdvancedCoder\Brend\Model\ResourceModel\Brend\CollectionFactory;
use AdvancedCoder\Brend\Model\ResourceModel\Brend as BrendResource;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Api\SearchCriteriaInterface;
use AdvancedCoder\Brend\Api\BrendSearchResultInterfaceFactory;

class BrendRepository implements BrendRepositoryInterface
{
    private CollectionFactory $collectionFactory;
    private BrendResource $productTypesResource;
    private BrendFactory $productTypesFactory;
    private BrendSearchResultInterfaceFactory $searchResultInterfaceFactory;
    private SearchCriteriaBuilder $searchCriteriaBuilder;

    /**
     * @param BrendFactory $productTypesFactory
     * @param CollectionFactory $collectionFactory
     * @param BrendResource $productTypesResource
     * @param BrendSearchResultInterfaceFactory $searchResultInterfaceFactory
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        BrendFactory $productTypesFactory,
        CollectionFactory $collectionFactory,
        BrendResource  $productTypesResource,
        BrendSearchResultInterfaceFactory $searchResultInterfaceFactory,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->productTypesFactory = $productTypesFactory;
        $this->collectionFactory = $collectionFactory;
        $this->productTypesResource = $productTypesResource;
        $this->searchResultFactory = $searchResultInterfaceFactory;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * @param int $id
     * @return BrendInterface
     * @throws NoSuchEntityException
     */
    public function get(int $id): BrendInterface
    {
        $object = $this->productTypesFactory->create();
        $this->productTypesResource->load($object, $id);
        if (! $object->getId()) {
            throw new NoSuchEntityException(__('Unable to find entity with ID "%1"', $id));
        }
        return $object;
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return BrendSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria = null): BrendSearchResultInterface
    {
        $collection = $this->collectionFactory->create();
        $searchCriteria = $this->searchCriteriaBuilder->create();

        if (null === $searchCriteria) {
            $searchCriteria = $this->searchCriteriaBuilder->create();
        } else {
            foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
                foreach ($filterGroup->getFilters() as $filter) {
                    $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
                    $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
                }
            }
        }

        $searchResult = $this->searchResultFactory->create();
        $searchResult->setItems($collection->getItems());
        $searchResult->setTotalCount($collection->getSize());
        $searchResult->setSearchCriteria($searchCriteria);
        return $searchResult;
    }

    /**
     * @param BrendInterface $productTypes
     * @return BrendInterface
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    public function save(BrendInterface $productTypes): BrendInterface
    {
        $this->productTypesResource->save($productTypes);
        return $productTypes;
    }

    /**
     * @param BrendInterface $workingHours
     * @return bool
     */
    public function delete(BrendInterface $workingHours): bool
    {
        try {
            $this->productTypesResource->delete($workingHours);
        } catch (\Exception $e) {
            throw new StateException(__('Unable to remove entity #%1', $workingHours->getId()));
        }
        return true;
    }

    /**
     * @param int $id
     * @return bool
     * @throws NoSuchEntityException
     */
    public function deleteById(int $id): bool
    {
        return $this->delete($this->get($id));
    }

}
