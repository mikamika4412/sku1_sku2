<?php

namespace AdvancedCoder\Brend\Model;

use AdvancedCoder\Brend\Api\BrendSearchResultInterface;
use Magento\Framework\Api\SearchResults;

class BrendSearchResult extends SearchResults implements BrendSearchResultInterface
{
}
