<?php

namespace AdvancedCoder\Brend\Model\ResourceModel;

use AdvancedCoder\Brend\Api\Data\BrendInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;

class Brend extends AbstractDb
{
    public const TABLE_NAME = 'advanced_coder_brend';

    /**
     * @param Context $context
     */
    public function __construct(
        Context $context
    ) {
        parent::__construct($context);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, BrendInterface::ID);
    }
}
