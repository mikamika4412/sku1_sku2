<?php

namespace AdvancedCoder\Brend\Model\ResourceModel\Brend;

use AdvancedCoder\Brend\Api\Data\BrendInterface;
use AdvancedCoder\Brend\Model\Brend;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use AdvancedCoder\Brend\Model\ResourceModel\Brend as BrendResource;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(Brend::class, BrendResource::class);
    }

    /**
     * Convert to option array
     *
     * @return array
     */
    public function toOptionArray()
    {
        return $this->_toOptionArray(BrendInterface::ID, BrendInterface::NAME);
    }
}
