<?php

namespace AdvancedCoder\Brend\Model\Source;

use AdvancedCoder\Brend\Model\ResourceModel\Brend\Collection;

class Brend extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    private Collection $collection;

    /** @var array */
    private $options;
    public static $images = [];

    public function __construct(
        Collection $collection
    ) {
        $this->collection = $collection;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        if (!$this->options) {
            $this->options = $this->collection->toOptionArray();
            $this->options[] = ['value' => '', 'label' => '---Select---'];
        }

        return $this->options;
    }

    /**
     * @return array
     */
    public function getAllOptions()
    {
        return $this->toOptionArray();
    }

}
