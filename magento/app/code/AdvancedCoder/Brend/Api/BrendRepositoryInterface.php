<?php

namespace AdvancedCoder\Brend\Api;

use AdvancedCoder\Brend\Api\Data\BrendInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

interface BrendRepositoryInterface
{
    /**
     * @param int $id
     * @return BrendInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get(int $id): BrendInterface;

    /**
     * @param SearchCriteriaInterface|null $searchCriteria
     * @return BrendSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria = null): BrendSearchResultInterface;

    /**
     * @param BrendInterface $productTypes
     * @return BrendInterface
     */
    public function save(BrendInterface $productTypes): BrendInterface;

    /**
     * @param BrendInterface $workingHours
     * @return bool
     */
    public function delete(BrendInterface $workingHours): bool;

    /**
     * @param int $id
     * @return bool
     */
    public function deleteById(int $id): bool;
}
