<?php

namespace AdvancedCoder\Brend\Api;

interface BrendSearchResultInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * @return \AdvancedCoder\Brend\Api\Data\BrendInterface[]
     */
    public function getItems();

    /**
     * @param \AdvancedCoder\Brend\Api\Data\BrendInterface[]
     * @return void
     */
    public function setItems(array $items);
}
