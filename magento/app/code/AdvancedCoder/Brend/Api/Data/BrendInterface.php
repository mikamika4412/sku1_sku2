<?php

namespace AdvancedCoder\Brend\Api\Data;

interface BrendInterface
{
    const ID = 'id';
    const NAME = 'name';

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @param string $name
     * @return void
     */
    public function setName(string $name): void;

    /**
     * @return mixed|null
     */
    public function getId();

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id);
}
