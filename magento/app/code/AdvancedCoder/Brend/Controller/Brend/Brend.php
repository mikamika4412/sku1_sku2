<?php

namespace AdvancedCoder\Brend\Controller\Brend;

use Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\ResultFactory;

class Brend extends Action
{

    /**
     * @return \Magento\Framework\Controller\ResultInterface|\Magento\Framework\App\ResponseInterface
     */
    public function execute()
    {
        return $this->resultFactory->create(ResultFactory::TYPE_PAGE);
    }
}
