<?php

namespace AdvancedCoder\Brend\Controller\Adminhtml\Brend;

use AdvancedCoder\Brend\Api\BrendRepositoryInterface;
use AdvancedCoder\Brend\Model\BrendFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use AdvancedCoder\Brend\Api\Data\BrendInterface;

class Save extends Action implements HttpPostActionInterface
{
    private BrendRepositoryInterface $productTypesRepository;
    private BrendFactory $productTypesFactory;

    /**
     * @param Context $context
     * @param BrendRepositoryInterface $productTypesRepository
     * @param BrendFactory $productTypesFactory
     */
    public function __construct(
        Context $context,
        BrendRepositoryInterface $productTypesRepository,
        BrendFactory $productTypesFactory
    ) {
        parent::__construct($context);
        $this->productTypesRepository = $productTypesRepository;
        $this->productTypesFactory = $productTypesFactory;
    }

    /**
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $request = $this->getRequest();
        $requestData = $request->getPost()->toArray();

        if (!$request->isPost() || empty($requestData['general'])) {
            $this->messageManager->addErrorMessage(__('Wrong request.'));
            $resultRedirect->setPath('*/*/new');
            return $resultRedirect;
        }

        try {
            $id = $requestData['general'][BrendInterface::ID];
            $productType = $this->productTypesRepository->get($id);
        } catch (\Exception $e) {
            $productType = $this->productTypesFactory->create();
        }

        $productType->setName($requestData['general'][BrendInterface::NAME]);
        try {
            $productType = $this->productTypesRepository->save($productType);
            $this->processRedirectAfterSuccessSave($resultRedirect, $productType->getId());
            $this->messageManager->addSuccessMessage(__('Brend was saved.'));

        } catch (\Exception $exception) {
            $this->messageManager->addErrorMessage(__('Error. Cannot save'));

            $resultRedirect->setPath('*/*/new');
        }

        return $resultRedirect;
    }

    /**
     * @param Redirect $resultRedirect
     * @param string $id
     * @return void
     */
    private function processRedirectAfterSuccessSave(Redirect $resultRedirect, string $id)
    {
        if ($this->getRequest()->getParam('back')) {
            $resultRedirect->setPath(
                '*/*/edit',
                [
                    BrendInterface::ID => $id,
                    '_current' => true,
                ]
            );
        } elseif ($this->getRequest()->getParam('redirect_to_new')) {
            $resultRedirect->setPath(
                '*/*/new',
                [
                    '_current' => true,
                ]
            );
        } else {
            $resultRedirect->setPath('*/*/');
        }
    }
}
