<?php

namespace AdvancedCoder\Brend\Controller\Adminhtml\Brend;

use AdvancedCoder\Brend\Model\BrendRepository;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;

class NewAction extends Action
{
    /**
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Advanced_Coder::product_types';
    private BrendRepository $productTypesRepository;

    public function __construct(
        Context $context,
        BrendRepository $productTypesRepository
    ) {
        parent::__construct($context);
        $this->productTypesRepository = $productTypesRepository;
    }

    /**
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        /** @var Page $result */
        $result = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $result->setActiveMenu('AdvancedCoder_Brend::advanced_coder')
            ->addBreadcrumb(__('New Brend'), __('Brend '));
        $result->getConfig()->getTitle()->prepend(__('New Brend'));

        return $result;
    }
}
