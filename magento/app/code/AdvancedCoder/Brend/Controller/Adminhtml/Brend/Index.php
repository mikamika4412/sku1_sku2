<?php

namespace AdvancedCoder\Brend\Controller\Adminhtml\Brend;

use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;

class Index extends Action
{
    /**
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Advanced_Coder::brend';

    /**
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        /** @var Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->setActiveMenu('AdvancedCoder_Brend::advanced_coder')
            ->addBreadcrumb(__('Brend'), __('List'));
        $resultPage->getConfig()->getTitle()->prepend(__('Brend'));

        return $resultPage;
    }
}
