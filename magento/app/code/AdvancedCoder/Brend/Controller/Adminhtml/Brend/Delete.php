<?php

namespace AdvancedCoder\Brend\Controller\Adminhtml\Brend;

use AdvancedCoder\Brend\Api\BrendRepositoryInterface;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;

class Delete extends Action implements HttpPostActionInterface
{

    private BrendRepositoryInterface $productTypesRepository;

    /**
     * @param Context $context
     * @param BrendRepositoryInterface $productTypesRepository
     */
    public function __construct(
        Context $context,
        BrendRepositoryInterface $productTypesRepository
    ) {
        parent::__construct($context);
        $this->productTypesRepository = $productTypesRepository;
    }

    /**
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $request = $this->getRequest();
        $productTypeId = (int)$this->getRequest()->getParam('id');

        if(!$productTypeId) {
            $this->messageManager->addErrorMessage(__('Error.'));
            return $resultRedirect->setPath('*/*/index');

        }

        try {
            $productType = $this->productTypesRepository->get($productTypeId);
            $this->productTypesRepository->delete($productType);
            $this->messageManager->addSuccessMessage(__('You deleted the brend.'));

        } catch (NoSuchEntityException $e) {
            $this->messageManager->addErrorMessage(__('Cannot delete brend'));

        }
        return $resultRedirect->setPath('*/*/index');
    }
}
