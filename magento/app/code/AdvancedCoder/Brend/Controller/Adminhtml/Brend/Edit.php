<?php

namespace AdvancedCoder\Brend\Controller\Adminhtml\Brend;

use AdvancedCoder\Brend\Model\BrendRepository;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;

class Edit extends Action
{
    /**
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Advanced_Coder::brend';
    private BrendRepository $productTypesRepository;

    /**
     * @param Context $context
     * @param BrendRepository $productTypesRepository
     */
    public function __construct(
        Context $context,
        BrendRepository $productTypesRepository
    ) {
        parent::__construct($context);
        $this->productTypesRepository = $productTypesRepository;
    }

    /**
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        $id = $this->getRequest()->getParam('id');
        try {
            $type = $this->productTypesRepository->get($id);

            /** @var Page $result */
            $result = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
            $result->setActiveMenu('AdvancedCoder_Brend::advanced_coder')
                ->addBreadcrumb(__('Edit BREND'), __('Brend'));
            $result->getConfig()
                ->getTitle()
                ->prepend(__('Edit Brend: %name', ['name' => $type->getName()]));
        } catch (NoSuchEntityException $e) {
            /** @var Redirect $result */
            $result = $this->resultRedirectFactory->create();
            $this->messageManager->addErrorMessage(
                __('Product type with id "%value" does not exist.', ['value' => $id])
            );
            $result->setPath('*/*');
        }

        return $result;
    }
}
