<?php

namespace AdvancedCoder\Brend\Block;

use Magento\Framework\View\Element\Template;
use AdvancedCoder\Brend\Model\ResourceModel\Brend\CollectionFactory;

class Brend extends Template
{

    public const PATH = 'http://msp2.com/media/catalog/product';

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;
    /**
     * Collection factory
     * @var CollectionFactory
     */
    private CollectionFactory $collectionFactory;

    /**
     * @param Template\Context $context
     * @param CollectionFactory $collectionFactory
     * @param array $data
     */

    public function __construct(
        Template\Context $context,
        CollectionFactory $collectionFactory,
        array $data = []
    ) {

        $this->collectionFactory = $collectionFactory;
        parent::__construct($context, $data);
    }

    /**
     * getItems
     * array $data = [] Collection
     */
    public function getItems(): array
    {
        return $this->collectionFactory->create()->getItems();
    }

}
