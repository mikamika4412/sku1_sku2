<?php

namespace AdvancedCoder\Brend\Block;

use AdvancedCoder\Brend\Model\ResourceModel\Brend\Collection;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

class StockLeft extends Template implements ArgumentInterface
{
    private Registry $registry;
    private StockRegistryInterface $stockRegistry;
    private Collection $collection;


    /**
     * @param Context $context
     * @param Registry $registry
     * @param StockRegistryInterface $stockRegistry
     * @param resource $resource
     * @param Collection $collection
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        Registry $registry,
        StockRegistryInterface $stockRegistry,
        Collection $collection,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->registry = $registry;
        $this->stockRegistry = $stockRegistry;
        $this->collection = $collection;
    }

    /**
     * @return int
     */
    public function getIdBrend()
    {
        $product = $this->getCurrentProduct();

        return $product->getData('brend_id');
    }

    /**
     * @return \Magento\Catalog\Model\Product
     */
    public function getCurrentProduct()
    {
        return $this->registry->registry('product');
    }

    /**
     * @return mixed
     */
    public function getAllSuperHeroes()
    {
        $id=$this->getIdBrend();
        if (!$id)
        $id=1;
        return $this->collection->addFilter('id', $id )->load()->getData()[0]['image'];
    }
}


