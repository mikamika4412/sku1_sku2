<?php

namespace AdvancedCoder\Brend\Block;

use AdvancedCoder\Brend\Model\BrendRepository;
use AdvancedCoder\Brend\Model\ResourceModel\Brend\Collection;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Block\Product\Context;
use Magento\Catalog\Helper\Output as OutputHelper;
use Magento\Catalog\Model\Layer\Resolver;
use Magento\Framework\Data\Helper\PostHelper;
use Magento\Framework\Url\Helper\Data;

class test extends \Magento\Catalog\Block\Product\ListProduct

{
    private StockLeft $stockLeft;
    private Collection $collection;

    /**
     * @param Context $context
     * @param PostHelper $postDataHelper
     * @param Resolver $layerResolver
     * @param CategoryRepositoryInterface $categoryRepository
     * @param Data $urlHelper
     * @param array $data
     * @param OutputHelper|null $outputHelper
     * @param StockLeft $stockLeft
     * @param Collection $collection
     */
    public function __construct(
        Context $context,
        PostHelper $postDataHelper,
        Resolver $layerResolver,
        CategoryRepositoryInterface $categoryRepository,
        Data $urlHelper,
        array $data = [],
        ?OutputHelper $outputHelper = null,
        StockLeft $stockLeft,
        Collection $collection,
        BrendRepository $collection2

    ) {
        $this->StockLeft = $stockLeft;
        parent::__construct(
            $context,
            $postDataHelper,
            $layerResolver,
            $categoryRepository,
            $urlHelper,
            $data,
            $outputHelper
        );
        $this->stockLeft = $stockLeft;
        $this->collection = $collection;
        $this->collection2 = $collection2;

    }

    /**
     * @param $brend_id
     * @return array|mixed|null
     */
    public function getIm($brend_id)
    {
        /** @var TYPE_NAME $brend_id */
        if (!$brend_id) {
            $brend_id = 1;
        }
        $c = $this->collection2->getList()->getItems()[$brend_id]->getData('image');
        return $c;
    }
}
